### In Plesk, execute with:
### bash _deploy.sh >> _deployment.log 2>&1

echo ----------
echo Starting at: $(date)

### Go to directory with cloned git repo
cd ~/deploy_docs.verlicht.one

### Set the path so LaTeX can be found
PATH=$PATH:/var/www/vhosts/verlicht.one/.phpenv/shims:/opt/plesk/phpenv/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/var/www/vhosts/verlicht.one/.TinyTeX/bin/x86_64-linux

### Delete old 'public' directory if it exists
#rm -rf public

echo Current directory: $(pwd)
echo Current '$PATH': $PATH
echo Running Quarto

### Render the site
/usr/local/bin/quarto render --to all

echo Finished Quarto. Deleting old contents...

### Delete all contents in public HTML directory
rm -rf ~/docs.verlicht.one/*.*
rm -rf ~/docs.verlicht.one/*
#rm -f ~/docs.verlicht.one/.htaccess

echo Deleted old contents. Copying new contents...

### Copy website
cp -RT public ~/docs.verlicht.one

### Copy .htaccess
#cp .htaccess ~/spark.opens.science

echo Finished at: $(date)
echo ----------
