# Introductie

## Inleiding

Het VERLICHT-monitoringsproject gaat over Heerlen-Noord. Wie nu opgroeit in Heerlen-Noord (55.000 inwoners) heeft gemiddeld veel minder kansen op een goed inkomen, een fatsoenlijke woning of om langer gezond te blijven dan in de rest van Nederland. VERLICHT is een project in het kader van het Nationaal Programma Heerlen-Noord en wordt getrokken door de Open Universiteit. In Nationaal Programma Heerlen-Noord werken burgers, bedrijven, overheden en maatschappelijke organisaties 25 jaar samen aan een betere toekomst voor Heerlen-Noord. Zodat het voor de levenskansen en gezondheid van de volgende generaties niet uitmaakt of je in Heerlen-Noord of ergens anders in Nederland wordt geboren. Gelijke kansen voor iedereen! 

Heerlen-Noord is een groot gebied, met complexe problematiek, en veel stakeholders en actoren die allemaal voortdurend aan de slag zijn met interventies om specifieke verbeteringen te realiseren. Bij dergelijke complexe systemen is er veel samenhang tussen de onderdelen, en wat wel of niet wordt gedaan in elk van de de vijf leefgebieden (de interventies) heeft invloed op de effectiviteit van andere activiteiten.

Toen Nationaal Programma Heerlen-Noord startte, was niet helder welke interventies in welke leefgebieden in welke combinatie en welke volgorde ingezet moeten worden om de doelen in Heerlen-Noord te bereiken. Dit ondanks dat de afgelopen decennia miljarden zijn geïnvesteerd in achtergestelde wijken. Dat betekent dat Nationaal Programma Heerlen-Noord deels in het donker moet sturen.

Door een goed systeem op te zetten voor monitoring op te zetten kunnen we zorgen dat we de komende jaren en decennia exponentieel meer gaan leren over welke combinaties effectief zijn, en zo kunnen we steeds beter sturen en komen we steeds dichter bij het halen van onze doelen.

### Overzicht van VERLICHT

De basis van VERLICHT bestaat uit vijf nauw verweven aders:

-   **Complexiteit**: Complexiteit erkennen, niet onnodig simplificeren, en voor ons laten werken
-   **Uitlichten**: Slim uit complexiteit selecteren om besluitvorming optimaal te ondersteunen
-   **Luisteren**: Perspectieven van burgers, professionals, en stakeholders serieus nemen en integreren
-   **Iteratie**: Met een iteratieve aanpak van monitoring en interventie steeds (bij)leren en beter sturen
-   **Synergie**: Flexibel indicatoren kiezen, data koppelen, en omzetten in betekenis en kennis

Hieronder wordt besproken hoe deze benadering aansluit op de leidende principes van de [Routekaart Heerlen-Noord](https://heerlen.bestuurlijkeinformatie.nl/Document/View/b176ade2-ffbf-4b82-8156-1fb03dc7182d)^[([Gearchiveerde versie in de Wayback Machine](https://web.archive.org/web/20240806113323/https://heerlen.bestuurlijkeinformatie.nl/Document/View/b176ade2-ffbf-4b82-8156-1fb03dc7182d))]. Daarna worden deze vijf aders geoperationaliseerd in de concrete uitwerking van VERLICHT.

### We leren van het verleden

Het eerste leidende principe van de Routekaart Heerlen-Noord is dat we willen leren van het verleden. Dit willen we niet alleen nu, maar ook in de toekomst. Leren van het verleden betekent onthouden wat er gebeurt. VERLICHT realiseert monitoring die flexibel is ingericht en kan worden aangepast aan wat we leren. Deze monitoring brengt over de komende jaren en decennia het complexe systeem dat Heerlen-Noord is in kaart. Op die manier kunnen we steeds beter leren van het verleden.

### We zien en benutten de kracht van Heerlen-Noord

Door de monitoring breder te trekken dan de individuele interventies worden op een natuurlijke manier de sterke punten in kaart gebracht en worden kansen zichtbaar. Bovendien zullen we leren hoe deze sterke punten verder bestendigt kunnen worden door de interventies die gaan worden ingezet.

### We betrekken de burgers

De inwoners van Heerlen-Noord, maar ook bijvoorbeeld de professionals die in Heerlen-Noord werken en de bestuurders van de alliantiepartners en andere organisaties in de regio, kennen Heerlen-Noord, weten wat er speelt, en hebben ervaringen en opvattingen over wat werkt, wat niet werkt, waarom, en wat ze eigenlijk graag zouden zien. Dit is een belangrijke informatiebron om het complexe systeem dat Heerlen-Noord is in kaart te brengen. Maar soms geloven (groepen) mensen andere dingen, die niet tegelijkertijd waar kunnen zijn. VERLICHT helpt met het blootleggen van aannames, zodat deze getoetst kunnen worden en gericht aanvullende expertises ingeroepen kunnen worden. Zo nemen we iedereen serieus en luisteren we oprecht, zonder de regie te verliezen.

### We committeren ons aan een generatielange aanpak

De multiproblematiek in Heerlen-Noord is ontstaan over generaties. En in de afgelopen decennia zijn er talloze interventies gepleegd. We starten dit project vanuit de erkenning dat we ook decennia nodig hebben om deze problemen op te lossen. Dit betekent dat monitoring moet passen bij een decennialange aanpak. We kunnen veel leren, maar dat vereist dat we flexibel blijven. In het uitgangspunt dat we willen leren van het verleden ligt besloten dat we erkennen dat er dus nog veel te leren is. Vanuit die gepaste bescheidenheid veronderstellen we dat we nu nog niet precies weten hoe de beste monitoring er uitziet. Slim monitoren betekent daarom flexibel monitoren: het gaat niet om de initiële selectie van indicatoren, maar om een aanpak van monitoring die is ingesteld op weloverwogen bijstelling van indicatoren en de verzameling van grote hoeveelheden gekoppelde informatie over tijd.

### We werken integraal over en door de vijf leefgebieden heen

Omdat Heerlen-Noord een complex systeem is vereist effectieve interventie inzicht in dat systeem en de wederzijdse invloeden. Een integrale aanpak is nodig, en dat betekent ook integrale monitoring. Indicatoren moeten niet alleen aan doelen en interventies worden gekoppeld, maar over tijd moeten we leren welke andere factoren belangrijk zijn, hoe interventies invloed hebben op factoren over de leefgebieden heen, en wat de relatieve voor- en nadelen van de beschikbare indicatoren zijn. Zo kunnen we met minimale belasting voor burgers en onze organisaties toch slim monitoren en sturen.

### We stellen doelen, nemen verantwoordelijkheid, werken samen en houden ons aan afspraken

We stellen niet alleen doelen, maar we stellen ook doelen bij. Einddoelen worden opgebroken in subdoelen, en naarmate we meer leren kunnen we over steeds tijd slimmer prioriteren in die subdoelen. Door samen informatie te delen en te koppelen en duidelijke afspraken te maken over interventies en indicatoren houden we de extra benodigde inzet tot een minimum terwijl we maximaal inzicht verwerven.

### We werken vanuit ons verhaal

Heerlen-Noord is een uniek gebied, met een eigen verhaal, geschiedenis, cohesie, en verbondenheid. Om hier recht aan te doen moeten we die rijke context respecteren in de monitoring. De indicatoren die we opnemen in de monitoring staan niet op zichzelf; ze representeren unieke lichtpuntjes die samen een groter universum vormen. Het doel van VERLICHT is om niet alleen data op te leveren, maar ook kennis en betekenis, verankerd in het unieke verhaal van Heerlen-Noord.

## VERLICHT in de praktijk

Omdat VERLICHT wordt opgezet vanuit de bescheiden erkenning dat we nog veel willen —en dus kunnen— leren, zijn flexibiliteit en schaalbaarheid belangrijk. Dat geldt zowel voor de praktische organisatie als voor de manier waarop data worden verzameld, opgeslagen, en gekoppeld.

### Technologieën en processen

VERLICHT gaat van start met de specificatie van wat nu al bekend is: de doelen, subdoelen, interventies, en indicatoren die zijn uitgewerkt in het Routeplan. Dit vormt vervolgens de basis voor een serie gesprekken waarin het netwerk wordt aangevuld. Deze gesprekken worden gevoerd met burgers, professionals, beslissers, beleidsmakers, experts, en andere sleutelfiguren (de Luisteren-ader van VERLICHT). De gesprekken worden opgenomen, getranscribeerd, en geanonimiseerd. Naast gesprekken wordt bestaande documentatie als bronmateriaal gebruikt, en er worden expert meetings en Delphi-procedures ingezet. Deze verzameling van bronnen is een van de manifestaties van de Iteratie-ader van VERLICHT: het verzamelen van documentatie en het houden van gesprekken is uitdrukkelijk een steeds voortdurend proces.

#### ROCK en QNA

Deze bronnen worden opgeslagen en systematisch gecodeerd met de zogenaamde Reproducible Open Coding Kit ([ROCK](https://rock.science)), waar bovendien machine-readable metadata wordt toegevoegd. In deze codering worden netwerkrelaties gespecificeerd volgens een innovatieve, aan de OU ontwikkelde methode: de Qualitative Network Approach ([QNA](https://doi.org/hwzj)). Deze benadering maakt het mogelijk om willekeurige typen relaties en willekeurige typen concepten te representeren. Door de bronnen op deze manier te coderen worden de aannames over de dynamiek in Heerlen-Noord, de manier waarop interventies verwacht worden te werken, de externe factoren en omgevingselementen die een rol spelen, op welke indicatoren effecten verwacht kunnen worden, en waar er interactie is tussen leefgebieden, inzichtelijk gemaakt.

Zo ontstaat uit elke bron een netwerk. Aan de knooppunten en takken in deze netwerken worden indicatoren gekoppeld. De verzamelde data worden in eerste instantie opgeslagen in Topic Maps. Deze maken het mogelijk om flexibel netwerken te specificeren, netwerken samen te voegen, en (meta)data aan knooppunten en verbindingen te koppelen. Een voorbeeld van zulke metadata is een gewicht voor een bepaalde indicator, een bepaald netwerk, of voor specifieke onderdelen van netwerken.

Deze netwerken worden gecombineerd in een overstijgend beeld van Heerlen-Noord (de Complexiteit ader). Dit overstijgende beeld wordt opgetrokken uit de individuele netwerken door procedures toe te passen om de knooppunten en takken te combineren, waarbij de eerder geregistreerde metadata zoals gewichten worden meegenomen. Het netwerk dat zo ontstaat zal snel te complex worden voor een mens om in zijn geheel te overzien. Daarom worden aan de hand van gekoppelde metadata steeds bepaalde delen van het netwerk uitgelicht (de Uitlichten ader). Op die manier kan steeds de juiste context worden getoond als basis voor besluiten.

#### Koppeling met bestaande indicatoren uit Heerlen-Noord

Aan de knopen en vertakkingen in de veelvoud aan losse netwerken en het geïntegreerde overstijgende netwerk worden indicatoren gekoppeld. Dit zullen deels indicatoren zijn die speciaal voor VERLICHT worden verzameld, maar voor een groot deel ook bestaande indicatoren die al worden verzameld door alliantiepartners of andere organisaties die actief zijn in de regio of nationaal. Hiervoor worden adapters ontwikkeld die ofwel het ophalen van data uit externe systemen mogelijk maken, ofwel het actief aanbieden van data uit externe bronsystemen.

#### Systematische "living reviews"

Naast informatie uit Heerlen-Noord zal ook informatie uit de wetenschappelijke literatuur worden gebruikt. Met het Modular, Extensible, Transparent, Accessible, Bootstrapped Extraction For Systematic Reviews pakket ([metabefor](https://metabefor.opens.science)) zullen een serie systematische reviews worden opgezet waarin de literatuur over relevante onderwerpen wordt bijgehouden. Dit pakket maakt het mogelijk om op een modulaire wijze steeds extra informatie uit de wetenschappelijke literatuur op een machine-readable wijze toegankelijk te maken. Door deze informatie slim te labelen met concepten uit Heerlen-Noord kan deze data zo verrijkt worden met wetenschappelijke inzichten.

#### Semantic MediaWiki

De basis data-architectuur voor VERLICHT wordt geïmplementeerd in een Semantic MediaWiki ([SMW](https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki)) instantie. Dit is een uitbreiding van het bekende MediaWiki platform (waar o.a. Wikipedia op draait) dat een volledig kennismanagementsysteem kan herbergen met naadloze integratie met andere databases. SMW beschikt over een veelvoud aan extensies om bijvoorbeeld data-invoer te faciliteren en curatie-processen te begeleiden. Bovendien ondersteunt SMW zowel het Resource Description Framework ([RDF](https://www.w3.org/RDF/)) als de SPARQL Protocol and RDF Query Language ([SPARQL](https://www.w3.org/TR/rdf-sparql-query/)) standaard, zodat krachtige queries ontworpen kunnen worden om beslissingen zo efficiënt mogelijk te ondersteunen. SMW leent zich goed voor de specificatie van systemen om beslissingen te ondersteunen.

#### Datavisualisatie

Aanvullend op beslissings-ondersteunings-systemen zal gebruik worden gemaakt van een veelheid aan datavisualisatietechnieken om de inzichten uit VERLICHT optimaal inzetbaar te maken in praktijksituaties. Een voorbeeld zijn de honingraatkaarten die worden gebruikt voor de presentatie van geospatiële data (i.e. data gemapped op geografische representaties). Hierbij worden deze data gerepresenteerd op een hoger niveau: Heerlen-Noord wordt geabstraheerd naar wijkniveau (14 honingraten) om het vergelijken van enerzijds veranderingen over tijd met betrekking tot streefwaarden inzichtelijk te maken, en anderzijds relatieve verdeling van de gebieden met betrekking tot een indicator (of een aggregaat van indicatoren).

Door zo met slimme visualisaties en presentaties de informatie uit heel Heerlen-Noord integraal te combineren wordt het mogelijk om goed geïnformeerde besluiten te nemen over zowel bestaande interventies als toekomstige interventies. Door bij het inzetten van interventies goed de relevante aannames in kaart te brengen via gesprekken en aan de hand van documentatie, en indicatoren te koppelen aan deze aannames, kunnen we steeds meer leren over die aannames. Bovendien wordt het ook mogelijk om nieuwe verbanden te zien. Bij beslissingen kan dus worden gestuurd op slim gestructureerde informatie, niet alleen uit het recente verleden maar ook verder terug, en niet alleen direct gerelateerd aan de betreffende voorliggende kwestie maar ook via minder vanzelfsprekende relaties. Door zo de producten van de monitoring slim te combineren wordt de som meer dan de optelsom der delen (de Synergie ader).

### Organisatie

VERLICHT is organisationeel belegd bij de Open Universiteit. Door VERLICHT niet te beleggen bij een van de Alliantiepartners of in het programmabureau van Heerlen-Noord is het risico dat er een schijn van belangenverstrengeling ontstaat kleiner.

Bovendien garandeert het beleggen van VERLICHT bij de Open Universiteit dat wordt gewerkt volgens de Nederlandse Gedragscode Wetenschappelijke Integriteit en haar vijf leidende principes (zorgvuldigheid, onafhankelijkheid, verantwoordelijkheid, eerlijkheid, en transparantie).

Tot slot ontsluit deze opzet de multidisciplinaire expertise die aanwezig is bij de Open Universiteit, bijvoorbeeld met betrekking tot psychologie (bijvoorbeeld gedragsverandering en implementatietrajecten), methodologie (bijvoorbeeld validatie van meetinstrumenten en het ontwikkelen of aanpassen van meetinstrumenten voor laaggeletterde en praktisch opgeleide mensen), en bètawetenschappen (bijvoorbeeld software engineering, data structures, semantic web, machine learning, en uncertainty reasoning).

Zo wordt VERLICHT onafhankelijk ingebed en goed toegerust om integraal te werken. Tegelijkertijd worden publicaties opgeleverd aan de Alliantie, en bepaalt zij of en onder welke voorwaarden publicaties (of bijvoorbeeld dashboards) openbaar worden gemaakt. De brondata en de broncode die deze data verwerken zullen openbaar zijn (behalve persoonsgegevens).

### Financiering

VERLICHT wordt gefinancierd door de Alliantie Heerlen-Noord en de Open Universiteit. Voor de Alliantie Heerlen-Noord vervult VERLICHT een centrale rol in de monitoring. Voor de Open Universiteit representeert deze opzet het enthousiasme waarmee de Open Universiteit een bijdrage wil leveren aan het succes van Heerlen-Noord.

Zoals eerder aangegeven is het belangrijk dat VERLICHT schaalbaar is. In eerste instantie wordt daarom klein gestart, zodat de Alliantie en de OU zich kunnen bezinnen op de beste structurele oplossing, bijvoorbeeld de gewenste balans tussen vaste financiering uit de respectieve eigen middelen en de meer riskante financiering op project-basis.

De benodigde middelen betreffen salaris (in totaal 0.8 FTE coördinatie en daarnaast 2 FTE uitvoering) en materiaalkosten (waaronder softwareontwikkeling en -licenties vallen).

### Bemensing

VERLICHTe monitoring vereist een bredere combinatie van competenties dan conventionele monitoring. Het monitoringsteam moet competenties bezitten met betrekking tot methodologie (specifiek met betrekking tot kwalitatieve methoden, kwantitatieve methoden, en systematische reviews), ontologieën en het semantic web, data science en big data, netwerkbenaderingen en complexe systemen, psychologie (specifiek gedragsverandering en gezondheidsbevordering), verbinden, bestuurlijke sensitiveit, en helder rapporteren en overbrengen van complexe informatie.

VERLICHT is ontwikkeld door twee mensen in wie deze competenties zijn verenigd, en zij zullen in eerste instantie verantwoordelijk zijn voor de aansturing. VERLICHT wordt gecoördineerd door Dennis Martens (coördinator monitoring, programmabureau Heerlen-Noord), Gjalt-Jorn Peters (universitair hoofddocent bij Methoden en Technieken, Faculteit Psychologie, Open Universiteit), en Lloyd Rutledge (universitair docent bij Informatiekunde, Faculteit Bètawetenschappen, Open Universiteit). Samen sturen zij het VERLICHT team aan, borgen ze de inbedding van VERLICHT in het gehele monitoringsprogramma van Heerlen-Noord, en zorgen ze dat de lijnen naar de Alliantiepartners en relevante stakeholders enerzijds, en hoogleraren en andere wetenschappers in de relevante domeinen anderzijds kort blijven. Zo wordt VERLICHT ingebed in zowel de Alliantie als verbinder en bestendiger enerzijds, en de Open Universiteit als hoeder van methodologie en theorie anderzijds.

De kern van het team bestaat uit Ivonne Lipsch-Wijnen en Hessel Poelman, die full-time voor VERLICHT werken. In totaal bestaat het volledige VERLICHT team uit 2.8 FTE, die in zich de competenties verenigen die nodig zijn om volgens de vijf aders van VERLICHT te werken. De taken die het team uitvoert zijn het opzetten en onderhouden van de data architectuur (Complexiteit), het ontwikkelen van gerichte rapportages (Uitlichten), het houden en coderen van de interviews, bijvoorbeeld met burgers, professionals, stakeholders, bestuurders, beleidsmakers, en experts (Luisteren), het identificeren en koppelen van indicatoren (Synergie), en het adviseren over het iteratief integreren van de informatie uit veelheid aan bronnen in het volledige complexe systeem (Iteratie).
